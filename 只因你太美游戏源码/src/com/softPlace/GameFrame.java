package com.softPlace;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @ddauthor:likang
 * @date:2022/11/14 10:39
 */
public class GameFrame extends JFrame {

    GameFrame(){
        //电视机
        setTitle(Settings.TITLE);
        setSize(Settings.width,Settings.height);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        //添加事件监听
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                int i =JOptionPane.showConfirmDialog(GameFrame.this,"关闭咯");
                if (i==0) System.exit(0);
            }
        });

        add(new GamePanel());
    }
    public void  getstart(){
        setVisible(true);
    }
}
