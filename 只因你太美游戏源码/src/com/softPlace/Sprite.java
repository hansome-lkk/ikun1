package com.softPlace;

import com.softPlace.Utils.ImgUtil;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author:likang
 * @date:2022/11/14 14:30
 */
public abstract class Sprite
{
    //渲染图片对象
    private BufferedImage img;
    //坐标
    private int x;
    private int y;
    private int hp = 1000;
    int mp = 1000;

    public int getMp() {
        return mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    //技能1
    String[] skill1={};

    public String[] getSkill1() {
        return skill1;
    }

    public void setSkill1(String[] skill1) {
        this.skill1 = skill1;
    }

    public String[] getSkill2() {
        return skill2;
    }

    public void setSkill2(String[] skill2) {
        this.skill2 = skill2;
    }

    //技能2
    String[] skill2={};

    private int width;
    private int height;

    public Sprite(){
        //模板方法模式，提供模板
        //父类并未new 出
        System.out.println(this.getClass().toString()+":调用init1");
        init();
    }

    private boolean death;
    public abstract void init();


    public static boolean changeFlag =false;
    public void draw(Graphics g){
        //1.panel上用
        // 2画自己
        if(!changeFlag)
        g.drawImage(img,x,y,width,height,null);
        else
            g.drawImage(img,x,y,width*2,height*2,null);
    }

    /*
    监测碰撞
    Para1:敌人信息
    Para2 :技能
     */
    boolean checkCrash(Sprite sprite,String[] strings){
        //获取技能攻击范围
        String string = strings[0];
        BufferedImage image = ImgUtil.load(string);
        int CrashWidth = image.getWidth();
        int CrasHeight = image.getHeight();

        if(Math.abs(sprite.getX()-this.getX())<CrashWidth*1.5
                && Math.abs(sprite.getY()-this.getY())<CrasHeight*1.5 ){
            //碰撞
            return true;
        }

        return false;
    }

    /*
    画攻击
     */
    abstract void drawAttack(Graphics g,String[] strings);

    /*
            攻击

            skill : 技能几
         */
    abstract void attack(Sprite sprite,String[] strings,int code);

    public abstract void destory();

    public BufferedImage getImg() {
        return img;
    }

    public void setImg(BufferedImage img) {
        this.img = img;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isDeath() {
        return death;
    }

    public void setDeath(boolean death) {
        this.death = death;
    }
}
