package com.softPlace.Utils;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.InputStream;

/**
 * 音效播放工具类
 */
public class SoundUtilsA {


    /**攻击音效*/
    public static final String ATTACK = "/music/bgm.mp3";

    /**死亡音效*/
    public static final String DIE = "/music/die.mp3";
    /**技能释放音效*/
    public static final String SKILL = "/music/skill1.mp3";

    /**声明播放器*/
    public static Player player;



    //销毁还是continue
    public static boolean flag =true;
    public static void play(String musicName){
        //启动音效播放线程
        Thread thread=new Thread(){
            @Override
            public void run() {
                do {
                    try {
                        //加载音乐资源为输入流
                        InputStream is = SoundUtilsA.class.getResourceAsStream(musicName);
                        //创建播放器对象
                        player = new Player(is);
                        //播放音乐
                        player.play();
                        flag=false;
                    } catch (JavaLayerException e) {
                        e.printStackTrace();
                    }
                }while(flag);
            }
        };
        if (flag){
        thread.start();
        }else {
            //停止
            thread.interrupt();
            flag=true;
        }


    }

    public static void main(String[] args) {
//        SoundUtils.play(SoundUtils.BG_MUSIC,true);

    }
}
