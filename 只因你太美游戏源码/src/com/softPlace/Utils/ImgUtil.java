package com.softPlace.Utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author:likang
 * @date:2022/11/14 13:56
 */
/*
//工具类：加载图片
 */
public class ImgUtil {

    /*
    工具类测试
     */
    public static void main(String[] args)  {


    }


    
    public  static BufferedImage load(String imgNmae)  {
        //反射
        InputStream resourceAsStream = ImgUtil.
                class.
                getResourceAsStream("/img/" + imgNmae);

        //返回为BufferImag对象
        BufferedImage read = null;
        try {
            read = ImageIO.read(resourceAsStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return read;


    }
}
