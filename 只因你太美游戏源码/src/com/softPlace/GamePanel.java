package com.softPlace;

import com.softPlace.Utils.ImgUtil;
import com.softPlace.Utils.SoundUtils;
import com.softPlace.Utils.SoundUtilsA;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.Calendar;
import java.util.HashSet;
import java.util.TimerTask;

/**
 * @author:likang
 * @date:2022/11/14 13:48
 */
public class GamePanel extends JPanel {
    static Hero hero=new Hero();
    static Hero2 hero2 = new Hero2();
    static boolean heroAttack1 =false ;
    static boolean heroAttack2 =false ;
    static boolean hero2Attack1 =false ;
    static boolean hero2Attack2 =false ;

    static int idex_map=0;
    java.util.Timer timer=new java.util.Timer();
    public static  String[] maps ={
            "bg0.jpg",
            "bg1.png",
            "bg2.png",
            "bg3.jpg",
            "bg4.png",
            "bg5.png",
            "bg6.png",
            "bg7.png",
            "bg8.png"

    };

    static void nextMap(){
        idex_map++;
    }
    static  void preMap(){
        idex_map--;
    }

    GamePanel(){



        //背景音乐
        SoundUtils.play(SoundUtils.BG_MUSIC);
        //创建任务
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                    repaint();
            }
        },0,150);

        //获取当前面包焦点
        setFocusable(true);
        //绑定监听
        addKeyListener(new KeyAdapter() {
            //声明set集合存储按下的多个按键（数据结构：散列表-hash表）  hashmap实现原理
            private HashSet<Integer> keys = new HashSet<>();
            @Override
            public void keyPressed(KeyEvent e) {

                int code=e.getKeyCode();
                //记录按键码到集合
                keys.add(code);
                /*
                输入：   w:87 s:83 a:65 d:68
                   @return 切换地图 0不切换  1切换下一个 -1上一个
                    切换地图 0不切换  1切换下一个 -1上一个
                 */
                keys.forEach(k->{
                    //执行玩家控制行为
                    hero.move(k);
                    hero2.move(k);
                    //hero1 技能
                    hero.attack(hero2,hero.skill1,k);
                    hero.attack(hero2,hero.skill2,k);
                    hero2.attack(hero,hero2.skill1,k);
                    hero2.attack(hero,hero2.skill2,k);
                });

            }
            /**按键抬起时触发以下方法*/
            @Override
            public void keyReleased(KeyEvent e) {
                int code = e.getKeyCode();
                //从集合中移除该按键
                keys.remove(code);
            }
        });


    }


    public void paint(Graphics g) {
        Calendar cal=Calendar.getInstance();
        boolean flag = false;
        if(!hero.isDeath() &&!hero2.isDeath())
            //两个英雄都没死
                {
                    int second = cal.get(Calendar.SECOND);

                    if(hero.hp+5<100){

                        if(second%10==0){
                            hero.setHp(hero.hp+5);
                        }
                    }
                    if(hero.mp+5<100){

                        if(second%10==0){
                            hero.setMp(hero.mp+5);
                        }
                    }
                    if(hero2.hp+5<100){

                        if(second%10==0){
                            hero2.setHp(hero2.hp+5);
                        }
                    }
                    if(hero2.mp+5<100){

                        if(second%10==0){
                            hero2.setMp(hero2.mp+5);
                        }
                    }
               /*
                一直刷新画
                 */
            //判断数组是否越界
            if(idex_map<0)
                idex_map=maps.length-1;

            if(idex_map>maps.length-1)
                idex_map=0;

            BufferedImage load = ImgUtil.load(maps[idex_map]);
            g.drawImage(load,0,0,null);

            if (heroAttack1){
                //改变图像大小
//                hero.changeFlag=true;
                //英雄1 攻击释放剂能1
                hero.drawAttack(g,hero.skill1);
                GamePanel.heroAttack1=false;
            }
            if (heroAttack2){
                //改变图像大小
                //英雄1 攻击释放剂能2
                hero.drawAttack(g,hero.skill2);
                GamePanel.heroAttack2=false;
            }
                    hero.draw(g);



            /*
            hero2
             */


            if (hero2Attack1){
                //改变图像大小
//                hero2.changeFlag=true;

                //英雄2 攻击释放剂能1
                hero2.drawAttack(g,hero2.skill1);

                //攻击音效
                SoundUtilsA.play(SoundUtilsA.ATTACK);

                GamePanel.hero2Attack1=false;
            }
            if (hero2Attack2){

                //改变图像大小
//                hero2.changeFlag=true;
                //英雄2 攻击释放剂能1
                hero2.drawAttack(g,hero2.skill2);

                //攻击音效
                SoundUtilsA.play(SoundUtilsA.ATTACK);

                GamePanel.hero2Attack2=false;

            }
            hero2.draw(g);
//            hero.changeFlag=false;

                }
        else if(!flag) {
             /*
                一直刷新画
                 */

            //判断数组是否越界
            if(idex_map<0)
                idex_map=maps.length-1;

            if(idex_map>maps.length-1)
                idex_map=0;

            BufferedImage load = ImgUtil.load(maps[idex_map]);
            g.drawImage(load,0,0,null);


            hero.draw(g);
            if (heroAttack1){
                //英雄1 攻击释放剂能1
                hero.drawAttack(g,hero.skill1);

                GamePanel.heroAttack1=false;
            }
            if (heroAttack2){
                //英雄1 攻击释放剂能2
                hero.drawAttack(g,hero.skill2);
                GamePanel.heroAttack2=false;
            }


            hero2.draw(g);
            if (hero2Attack1){
                //英雄2 攻击释放剂能1
                hero2.drawAttack(g,hero2.skill1);
                GamePanel.hero2Attack1=false;
            }
            if (hero2Attack2){
                //英雄2 攻击释放剂能2
                hero2.drawAttack(g,hero2.skill2);
                GamePanel.hero2Attack2=false;

            }
            //某一个死亡
            flag=true;
            BufferedImage image = ImgUtil.load("gameover1.png");
            g.drawImage(image,Settings.width/4,Settings.height/4,null);
        }
        else {
            BufferedImage image = ImgUtil.load("gameover1.png");
            g.drawImage(image,Settings.width/4,Settings.height/4,null);

        }
    }
}
