package com.softPlace;

import com.softPlace.Utils.ImgUtil;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author:likang
 * @date:2022/11/15 12:41
 */
public class Hero2 extends Sprite implements Runnable{

    int hp = 100;
    int mp = 100;

    //技能1  "kunkundazhao.gif",
    String[] skill1={
            "cxk/BossBoom_0.png",
            "cxk/BossBoom_1.png",
            "cxk/BossBoom_2.png",
            "cxk/BossBoom_3.png",
    };
    //技能2
    String[] skill2={"boom.gif",
            "boom.gif",
            "boom.gif",
    };
//    public String dir = "R";
    private  int speed=20;
    int level =1;
    int exp =0;

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMp() {
        return mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    /*
        固定在屏幕中
         */
    void fixed(){
        int x = this.getX();
        int y = this.getY();
        if (x>Settings.width){
            //超过左边距，从右边出来
            this.setX(0);
            GamePanel.preMap();

        }
        if (x<0){
            //超过右边距
            this.setX(Settings.width);
            GamePanel.nextMap();

        }
        if (y<Settings.height/2-100){
            //超过下
            setY(Settings.height/2-100);

        }
        if (y>593){
            //超过上
            setY(593);

        }


    }
    /*
    移动方法
     */
    void  move(int code ){
        switch (code){
            case 38:
                //上
                setY(getY()-speed);
                fixed();

                break;
            case 40:
                //下
                setY(getY()+speed);
                fixed();

                break;
            case 37:
                //左

                setX(getX()-speed);
                fixed();
                break;
            case 39:

                setX(getX()+speed);
                fixed();
                break;
            default:
                //其他键
        }


    }


    Timer timer ;

    Hero2(){
        System.out.println(this.getClass().toString()+"创建");
    }

    @Override
    public void init() {
        setX(Settings.width-100);
        setY(Settings.height/2);

        this.setImg(ImgUtil.load("/cxk/paddle_1.png"));

        this.setWidth(getImg().getWidth());
        this.setHeight(getImg().getHeight());
        //初始化玩家坐标
        setY(Settings.height/2);

        timer = new Timer();
        timer.schedule(new TimerTask() {
            private  int index=0;
            @Override
            public void run() {
                index=++index%3;
                setImg(ImgUtil.load("/cxk/paddle_" +index+".png"));
            }
        }, 0, 150);

    }


    @Override
    public void destory() {

    }

    @Override
    public void run() {

    }
    @Override
    public void draw(Graphics g) {
        //画自己（）
        super.draw(g);

        g.setColor(Color.red);
        g.drawRect(Settings.width-Settings.width/4,0,Settings.width/4,50);
        g.fillRect(Settings.width-Settings.width/4,0,  (int)((getHp()/100.0)*Settings.width/4),
                50);


        //画蓝条
        g.setColor(Color.blue);
        g.drawRect(Settings.width-Settings.width/4,51,Settings.width/4,50);
        g.fillRect(Settings.width-Settings.width/4 ,51,
                (int)((getMp()/100.0)*Settings.width/4),50);
    }


    /*
    画攻击
     */
    public void drawAttack(Graphics g,String[] strings){
        int index=0;
        int len = strings.length;
        while (index<=len-1){
            BufferedImage image = ImgUtil.load(strings[index++]);

            g.drawImage(image,getX(),getY(),null);

        }


    }


    /*
     攻击
      */
    void attack(Sprite sprite,String[] strings,int code) {

        int enemyHp = sprite.getHp();
        int enemyMp = this.getMp();

        //监测碰撞监测
        boolean crash = checkCrash(sprite, strings);

            if(sprite.getHp()>0) {
                //敌人活着 f是技能1（攻击）不耗蓝
                switch (code) {
                    case 77:
                        //技能1 f
                        //释放
                        GamePanel.hero2Attack1 = true;
                        if (crash) {
                            //碰撞了
                            sprite.setHp(enemyHp - 2);
                            System.out.println(this.getClass().toString() + "释放技能1:" + code +
                                    ",当前血量：" + sprite.getHp());
                        }
                        break;

                    case 78:
                        GamePanel.hero2Attack2 = true;
                        if (crash) {
                            //碰撞了
                            //技能2 g
                            sprite.setHp(enemyHp - 2);
                            this.setMp(enemyMp - 5);

                            System.out.println(this.getClass().toString() + "释放技能2:" + code +
                                    ",当前血量：" + sprite.getHp());
                        }
                        break;

                    //技能其他。。。。

                }
            }
            else {
                //敌人死了
                sprite.setDeath(true);
            }



    }



}



