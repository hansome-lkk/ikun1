package com.softPlace;

import com.softPlace.Utils.ImgUtil;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author:likang
 * @date:2022/11/14 15:17
 */
public class Hero extends Sprite{

    int hp = 100;
    int mp = 100;

    //技能1
    String[] skill1={"gykill.gif",
            "gykill.gif",
            "gykill.gif"};
    //技能2
    String[] skill2={"gy_run.gif","gy_run.gif","gy_run.gif"};

    public String dir = "R";

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMp() {
        return mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    private  int speed=19;
    int level =1;
    int exp =0;


    /*
    固定在屏幕中
     */
        void fixed(){
            int x = this.getX();
            int y = this.getY();
            if (x>Settings.width){
                //超过左边距，从右边出来
                this.setX(0);
                GamePanel.preMap();

            }
            if (x<0){
                //超过右边距
                this.setX(Settings.width);
                GamePanel.nextMap();

            }
            if (y<Settings.height/2-100){
                //超过下
                setY(Settings.height/2-100);

            }
            if (y>593){
                //超过上
                setY(593);

            }


        }
    /*
    移动方法
     */
    void  move(int code ){
        switch (code){
            case 87:
                //上
                setY(getY()-speed);
                fixed();

                break;
            case 83:
                //下
                setY(getY()+speed);
                fixed();

                break;
            case 65:
                //左
                this.dir="L";
                setX(getX()-speed);
                fixed();
                break;
            case 68:
                this.dir="R";
                setX(getX()+speed);
                fixed();
                break;
            default:
            //其他键
        }


    }

    /*
    画攻击
     */
    public void drawAttack(Graphics g,String[] strings){
        int index=0;
        int len = strings.length;
       while (index<=len-1){
           BufferedImage image = ImgUtil.load(strings[index++]);
           g.drawImage(image,getX(),getY(),null);
       }


    }


    /*
    攻击
     */
    void attack(Sprite sprite,String[] strings,int code) {

        int enemyHp = sprite.getHp();
        int enemyMp = this.getMp();
        //监测碰撞监测
//        System.out.println("hero:"+code);
        boolean crash = checkCrash(sprite, strings);

            //碰撞了
            if(sprite.getHp()>0) {
                //敌人活着 f是技能1（攻击）不耗蓝
                switch (code) {
                    case 71:
                        GamePanel.heroAttack1 = true;
                        if (crash) {
                            //技能1 f
                            sprite.setHp(enemyHp - 2);
                            this.setMp(enemyMp - 2);
                            System.out.println(this.getClass().toString() + "释放技能1:" + code +
                                    ",当前血量：" + sprite.getHp());

                        }
                        break;

                    case 70:
                        GamePanel.heroAttack2 = true;
                        if (crash) {
                            //技能2 g
                            sprite.setHp(enemyHp - 2);
                            this.setMp(enemyMp - 2);
                            System.out.println(this.getClass().toString() + "释放技能2:" + code +
                                    ",当前血量：" + sprite.getHp());

                        }
                        break;
                    //技能其他。。。。

                }
            }
            else {
                //敌人死了
                sprite.setDeath(true);
                System.out.println(sprite.getClass().toString()+"死亡！");
            }



    }


    Hero(){
        System.out.println(this.getClass()+"创建");

    }
    Timer timer ;

    @Override
    public void init() {

        //初始化图像信息
        this.setImg(ImgUtil.load("h_R0.png"));

        this.setWidth(getImg().getWidth());
        this.setHeight(getImg().getHeight());
        //初始化玩家坐标
        setY(Settings.height/2);

         timer = new Timer();
         //定时器自动开启不动时，一直有效果
        timer.schedule(new TimerTask() {

            private  int index;
            @Override
            public void run() {
                index=++index%8;
                setImg(ImgUtil.load("h_"+dir+index+".png"));
            }
        }, 0, 200);

    }


    @Override
    public void destory() {

    }

    static int index=0;
    @Override
    //画自己（），和血条
    public void draw(Graphics g) {
        //画自己（）
        super.draw(g);

        //画血条
        g.setColor(Color.red);
        g.drawRect(0,0,Settings.width/4,50);
        g.fillRect(0,0,(int)((this.getHp()/100.0)*Settings.width/4),50);


        //画蓝条
        g.setColor(Color.blue);
        g.drawRect(0,51,Settings.width/4,50);

        g.fillRect(0,51,(int)((this.getMp()/100.0)*Settings.width/4),50);
    }


}
